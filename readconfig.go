package readconfig

import (
	"errors"
	"net/url"
)

var drivers map[string]func(url *url.URL) ([]byte, error) = map[string]func(url *url.URL) ([]byte, error){}

//ErrDriverNotFound occurs when the scheme is not supported
var ErrDriverNotFound = errors.New("No such dirver for the scheme")

//ReadConfig read config from address given as an URL
func ReadConfig(url *url.URL) ([]byte, error) {
	if h, exist := drivers[url.Scheme]; exist {
		return h(url)
	}
	return nil, ErrDriverNotFound
}
