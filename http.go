package readconfig

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

func init() {
	drivers["http"] = httpRead
}

func httpRead(url *url.URL) ([]byte, error) {
	resp, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
