package readconfig

import (
	"errors"
	"net/url"
	"os"
)

func init() {
	drivers["env"] = envRead
}

//ErrEnvNotExist occurs when the specific env var is not exist
var ErrEnvNotExist = errors.New("Specific env var is not exist")

func envRead(url *url.URL) ([]byte, error) {
	envKey := url.Host
	str, ok := os.LookupEnv(envKey)
	if !ok {
		return nil, ErrEnvNotExist
	}
	return []byte(str), nil
}
