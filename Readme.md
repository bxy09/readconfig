# ReadConfig

This library implements multiple driver to read some contents from multiple sources. Including: 
1. http http://host/path
2. the local file file:///etc/
3. the environment variable env://ENVV
4. the etcd storage etcd://host1:port1/path?method=http&endpoints={encoded settings}
5. the redis key redis://host:port/key

