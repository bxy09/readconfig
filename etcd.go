package readconfig

import (
	"context"
	"github.com/coreos/etcd/client"
	"net/url"
	"strings"
	"time"
)

func init() {
	drivers["etcd"] = etcdRead
}

func etcdRead(url *url.URL) ([]byte, error) {
	query := url.Query()
	epStr := query.Get("endpoints")
	var ep []string
	if len(epStr) > 0 {
		ep = strings.Split(epStr, ",")
	} else {
		scheme := query.Get("scheme")
		if len(scheme) == 0 {
			scheme = "http"
		}
		ep = []string{scheme + "://" + url.Host}
	}
	cfg := client.Config{
		Endpoints:               ep,
		Transport:               client.DefaultTransport,
		HeaderTimeoutPerRequest: time.Second,
	}
	c, err := client.New(cfg)
	if err != nil {
		return nil, err
	}
	kapi := client.NewKeysAPI(c)
	resp, err := kapi.Get(context.Background(), url.Path, nil)
	if err != nil {
		return nil, err
	}
	return []byte(resp.Node.Value), nil

}
