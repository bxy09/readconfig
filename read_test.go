package readconfig_test

import (
	"context"
	"fmt"
	"github.com/coreos/etcd/client"
	"github.com/garyburd/redigo/redis"
	"gitlab.com/bxy09/readconfig"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"testing"
	"time"
)

func TestClients(t *testing.T) {
	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "127.0.0.1"
	}
	etcdHost := os.Getenv("ETCD_HOST")
	if etcdHost == "" {
		etcdHost = "127.0.0.1"
	}
	etcdAddr := fmt.Sprintf("http://%s:4001", etcdHost)
	param := url.Values{}
	param.Add("endpoints", etcdAddr)
	pEncode := param.Encode()
	paths := []string{
		"env://keykey",
		"file:///tmp/keykey",
		"http://127.0.0.1:8871/keykey",
		fmt.Sprintf("redis://%s:6379/keykey", redisHost),
		fmt.Sprintf("etcd://%s:4001/keykey", etcdHost),
		fmt.Sprintf("etcd://%s:4009/keykey?%s", etcdHost, pEncode),
	}
	key := "keykey"
	value := "You are right"
	//Prepare env
	os.Setenv(key, value)
	//Prepare file
	err := ioutil.WriteFile("/tmp/"+key, []byte(value), os.FileMode(0777))
	if err != nil {
		t.Fatal(err)
	}
	//Prepare http
	go func() {
		http.HandleFunc("/"+key, func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(value))
			return
		})
		err := http.ListenAndServe("127.0.0.1:8871", nil)
		if err != nil {
			t.Fatal(err)
		}
	}()
	//Prepare Redis
	redisClient, err := redis.Dial("tcp", redisHost+":6379")
	if err != nil {
		t.Fatal(err)
	}
	_, err = redisClient.Do("Set", key, value)
	if err != nil {
		t.Fatal(err)
	}
	//Prepare etcd
	c, err := client.New(client.Config{
		Endpoints:               []string{etcdAddr},
		Transport:               client.DefaultTransport,
		HeaderTimeoutPerRequest: time.Second,
	})
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.NewKeysAPI(c).Set(context.Background(), key, value, nil)
	if err != nil {
		t.Fatal(err)
	}

	for _, path := range paths {
		t.Run(path, func(t *testing.T) {
			URL, err := url.Parse(path)
			if err != nil {
				t.Fatal(err)
			}
			bytes, err := readconfig.ReadConfig(URL)
			if err != nil {
				t.Fatal(err)
			}
			if string(bytes) != value {
				t.Fatal("Not correct")
			}
		})
	}
}
