package readconfig

import (
	"io/ioutil"
	"net/url"
)

func init() {
	drivers["file"] = fileRead
}

func fileRead(url *url.URL) ([]byte, error) {
	return ioutil.ReadFile(url.Path)
}
