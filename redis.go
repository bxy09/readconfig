package readconfig

import (
	"github.com/garyburd/redigo/redis"
	"net/url"
	"strings"
)

func init() {
	drivers["redis"] = redisRead
}

func redisRead(url *url.URL) ([]byte, error) {
	conn, err := redis.Dial("tcp", url.Host)
	if err != nil {
		return nil, err
	}
	return redis.Bytes(conn.Do("GET", strings.TrimPrefix(url.Path, "/")))
}
